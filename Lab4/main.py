from typing import Optional

from fastapi import FastAPI, Form, File, UploadFile
from pydantic import BaseModel

class Student(BaseModel):
    name: str
    npm: str
    major: str
    year: str

student_db = {
    "1801": Student(
        name = "Ganyu",
        npm = "1801",
        major = "Computer Science",
        year = "2018"
    )
}

app = FastAPI()

def same_length(str1: str, str2: str):
    return {"check": len(str1) == len(str2)}

def empty_string_check(string: str):
    if (string is not None):
        if (string.strip):
            return True
    return False

@app.get("/")
def read_root():
    return {"Hello": "World"}

# Instruksi 1
@app.post("/create")
async def create(name: str = Form(...), npm: str = Form(...), major: str = Form(...), year: str = Form(...)):
    new_student = Student(name=name, npm=npm, major=major, year=year)
    student_db[npm] = new_student
    return new_student

@app.get("/{npm}")
async def read(npm: str):
    if (npm not in student_db):
        return {"message" : "NPM not in database!"}
    
    return student_db[npm]

@app.put("/{npm}")
async def update(npm: str, name: Optional[str] = Form(None), major: Optional[str] = Form(None), year: Optional[str] = Form(None)):
    if (npm not in student_db):
        return {"message" : "NPM not in database!"}

    target = student_db[npm]

    if (empty_string_check(name)):
        target.name = name

    if (empty_string_check(major)):
        target.major = major

    if (empty_string_check(year)):
        target.year = year

    student_db[npm] = target
    return target

@app.delete("/delete/{npm}")
async def delete(npm: str):
    if (npm not in student_db):
        return {"message" : "NPM not in database!"}

    student_db.pop(npm)

    return {"message" : f"Student with NPM {npm} has been deleted from the database."}

@app.post("/upload")
async def upload_file(file: UploadFile):
    return {'filename' : file.filename, 'content-type': file.content_type}